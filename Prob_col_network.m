%   EVOL_COLPROB_TD In this file, we evaluate the probability of collision 
%   in a pure ALOHA network in which the acknowledgement is sent if the channel
%   is free. This evaluation is done using either iterative computations or using
%   the analytical formulas derived in our article.

%   Other m-files required: none

%   Subfunctions: none
%   MAT-files required: none
%
%   See also : none

%   This file is the code used for the simulations done in the paper:
%   Bonnefoi, R.; Moy, C.; Palicot, J. “Improvement of the LPWAN AMI Backhaul's Latency 
%   thanks to Reinforcement Learning Algorithms”,  Eurasip JWCN, 2018. 

%   Author  : Rémi BONNEFOI
%   SCEE Research Team - CentraleSupélec - Campus de Rennes
%   Avenue de la Boulaie 35576 Rennes CEDEX CS 47601 FRANCE
%
%   Email : remi.bonnefoi@centralesupelec.fr
%   Website : remibonnefoi.wordpress.com
%
%   Last revision : 01/30/2018

% Time stamp
Time_length                 = 50000000; % 50000000
Sample_time                 = 1;
Tm                          = 40;
Td                          = 100;
Ta                          = 20;

% End-devices
Nd                          = 100:100:1000;         % Number of end-devices
                                    
p                           = 0.0001;
lambda                      = p/(Tm);

success                     = zeros(1,length(Nd));
fail                        = zeros(1,length(Nd));

for k=1:1:length(Nd) 
    disp(Nd(1,k))
    % Time space
    Time_space                  = zeros(1,Time_length+Tm);
    Index                       = zeros(1,Time_length+Tm);
    ack                         = zeros(1, Time_length+Tm);

    End_wait                    = zeros(1,Nd(1,k)); % Stop waiting for the acknowledgement

    State_IoT                   = zeros(1,Nd(1,k)); % State of the devices 0 : wait event
                                               %                      1 : wait acknowledgement 
    
    for i=1:1:Time_length

        % end-devices send packets
        proba                       = poissrnd(lambda,1,Nd(1,k));

        % Necessary conditions to send a packet
        cond1                       = State_IoT==0;
        cond2                       = cond1 & proba==1;

        sending                     = find(cond2);

        % Sending the packet
        for j=1:1:length(sending)
            End_wait(1,sending(1,j))               = i+Td+Tm+Ta-1;
            State_IoT(1,sending(1,j))              = 1;
        end


        if length(sending)==1,
            Time_space(1,i:i+Tm-1)      = Time_space(1,i:i+Tm-1)+1;
            Index(1,i:i+Tm-1)           = sending;
        elseif length(sending)>1,
            Time_space(1,i:i+Tm-1)      = Time_space(1,i:i+Tm-1)+2;
        end

        % The base station sends an acknowledgement
        if i>Tm+Td-1,
            cond1                        = sum(Time_space(i-Tm-Td+1:i-Td))==Tm;
            cond2                        = cond1 & sum(Index(i-Tm-Td+1:i-Td)==Index(1,i-Td))==Tm;
            cond3                        = cond2 & Time_space(1,i)==0;
            cond4                        = cond3 & sum(ack(i-Tm-Td+1:i-Td)==0)==Tm;
        else
            cond4                        = false;
        end

        % Send acknowledgement
        if cond4,
            Time_space(1,i+1:i+Ta)   = Time_space(1,i+1:i+Ta)+1;
            ack(1,i+1:i+Ta)          = 1;
            Index(1,i+1:i+Ta)        = Index(1,i-Td);
        end

        % An IoT comes back to state 0
        cond1                       = End_wait==i;
        receiving                   = find(cond1);


        for j=1:1:length(receiving)
            condr1                  = sum(Time_space(1,i-Ta+1:i))==Ta;
            condr2                  = condr1 & sum(Index(1,i-Ta+1:i)==receiving(1,j))==Ta;
            condr3                  = condr2 & sum(ack(1,i-Ta+1:i))==Ta;

            % In any cases the end-device is reset
            State_IoT(1,receiving(1,j))              = 0;
            End_wait(1,receiving(1,j))               = 0;


            if condr3,
               success(1,k)             = success(1,k)+1;
            else
                fail(1,k)               = fail(1,k)+1;
            end
        end 

    end
end

% practical probability
Pprac           = success./(fail+success);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Theoretical computation   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Tq              = Tm+Td+Ta;
lambda_T        = (Nd)*lambda;

% Td>Tm
if Td>Tm,

    Num             = exp(-2*lambda_T*Tm).*exp(-lambda_T*(Tm+Ta));
    Den             = 1 +(exp(-lambda_T*(Tm))-exp(-lambda_T*(Tm+Ta))).*(exp(-lambda_T*Td)+(1./(lambda_T*Ta)).*(exp(-lambda_T*(Tm))-exp(-lambda_T*(Tm+Ta))-exp(-lambda_T*(Td))+exp(-lambda_T*(Td+Ta))));

    Pth             = Num./Den;
else
% Td<Tm

    Num             = exp(-lambda_T*(2*Tm+Td+Ta));
    Den             = 1+exp(-lambda_T*(Tm+Td))-exp(-lambda_T*(Tm+Td+Ta));

    Pth             = Num./Den;
end


figure;
box on;
grid on;
hold on;
plot(lambda_T*Tm,Pprac,'o');
%plot(lambda_T*Tm,Pth2,'-');
plot(lambda_T*Tm,Pth,'-');
legend('Simulation results','Analytical derivation');
xlabel('\lambda_TT_m');
ylabel('Probability of success');
set(gca,'FontSize',13)
xlim([0.01 0.1]);